package jp.alhinc.kinase_ryota.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class CalculateSales {
	public static void main(String[] args) {
		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
		}

		String directoryPath = args[0];
		String inputBranch = "branch.lst";
		String inputCommodity = "commodity.lst";
		String outputBranch = "branch.out";
		String outputCommodity = "commodity.out";

		//(支店コード,支店名)
		HashMap<String, String> branchNames = new HashMap<String, String>();
		//(支店コード,売上）
		HashMap<String, Long> branchSale = new HashMap<String, Long>();
		//(商品コード,商品名)
		HashMap<String, String> commodityNames = new HashMap<String, String>();
		//(商品コード,売上)
		HashMap<String, Long> commoditySale = new HashMap<String, Long>();


		//branch.lstの読み込み
		if(!(fileInput(directoryPath, inputBranch, branchNames, branchSale, "^[0-9]{3}", "支店"))) {
			return;
		}

		//commodity.lstの読み込み
		if(!(fileInput(directoryPath, inputCommodity, commodityNames, commoditySale, "^[0-9a-zA-Z]{8}", "商品"))) {
			return;
		}


		//売上ファイルの取り出し
		//売上集計ファイル内のすべてのファイル
	    File[] files = new File(args[0]).listFiles();
	    //rcdファイルのみのリスト
		ArrayList<File> rcdFiles = new ArrayList<File>();
		for(int i = 0; i < files.length; i++) {
			if(files[i].isFile() && files[i].getName().matches("^[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}
		for(int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0,8));

			if((latter - former) != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}

		//売上ファイル読み込んでから集計してMapに格納
		BufferedReader br = null;
		for(int i = 0; i < rcdFiles.size(); i++) {
			try {
				// 売上ファイルの読込
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);
				String line;
				//{支店コード, 売上}
				ArrayList<String> rcdContents = new ArrayList<String>();
				while((line = br.readLine()) != null) {
					rcdContents.add(line);
				}

				if(rcdContents.size() != 3) {
					System.out.println(rcdFiles.get(i).getName() + "のフォーマットが不正です");
					return;
				}

				//売上ファイルの集計
				String branchCode = rcdContents.get(0);
				String commodityCode = rcdContents.get(1);
				if(!branchNames.containsKey(branchCode)) {
					System.out.println(rcdFiles.get(i).getName() + "の支店コードが不正です");
					return;
				}
				if(!commodityNames.containsKey(commodityCode)) {
					System.out.println(rcdFiles.get(i).getName() + "の商品コードが不正です");
					return;
				}

				if(!rcdContents.get(2).matches("^[0-9]*$")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

				long sale = Long.parseLong(rcdContents.get(2));
				long branchSalesAmount = branchSale.get(branchCode) + sale;

				long commoditySalesAmount = commoditySale.get(commodityCode) + sale;

				if((branchSalesAmount >= 10000000000L) || (commoditySalesAmount >= 10000000000L)) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				branchSale.put(branchCode, branchSalesAmount);
				commoditySale.put(rcdContents.get(1), commoditySalesAmount);
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				if(br != null) {
					try {
						br.close();
					} catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}

		//branch.outの出力
		if(!(fileOutput(directoryPath, outputBranch, branchNames, branchSale))) {
			return;
		}

		//commodity.outの出力
		if(!(fileOutput(directoryPath, outputCommodity, commodityNames, commoditySale))) {
			return;
		}
	}


	//読込メソッド
	public static boolean fileInput(String directoryPath, String fileName, HashMap<String, String> namesMap,
			HashMap<String, Long> saleMap, String fileNameFormat, String fileDefinition) {
		BufferedReader br = null;
		try {
			File file = new File(directoryPath, fileName);
			if(!file.exists()) {
				System.out.println(fileDefinition + "定義ファイルが存在しません");
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;
			while((line = br.readLine()) != null) {
				String[] items = line.split(",");
				if((items.length != 2) || (!items[0].matches(fileNameFormat))) {
					System.out.println(fileDefinition + "定義ファイルのフォーマットが不正です");
					return false;
				}
				namesMap.put(items[0], items[1]);
				saleMap.put(items[0], 0L);
			}
		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}



	//出力メソッド
	public static boolean fileOutput(String directoryPath, String fileName,
			HashMap<String, String> namesMap, HashMap<String, Long> saleMap){
		BufferedWriter bw = null;
		try {
			File file = new File(directoryPath,fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);
			for(String key : namesMap.keySet()) {
				bw.write(key + "," + namesMap.get(key) + "," + saleMap.get(key));
				bw.newLine();
			}
		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}
